package com.salikutluk.smarthome.commandline;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommandLineExecuter {

	private static SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss:SSS");

	public String executeCommand(String... commands) {
		StringBuffer output = new StringBuffer();
		try {
			log(commands);
			Process p = Runtime.getRuntime().exec(commands);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		log(output.toString());
		return output.toString();
	}

	private synchronized void log(String... messages) {
		String command = "";
		for (String message : messages) {
			command += " " + message;
		}
		System.out.println(format.format(new Date()) + ": " + command);
	}
}
