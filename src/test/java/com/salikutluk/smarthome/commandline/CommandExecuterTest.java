package com.salikutluk.smarthome.commandline;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class CommandExecuterTest {

	private static final String TEST_STRING = "Hello World";

	@Test
	public void testEchoHelloWorld() throws IOException, InterruptedException {
		CommandLineExecuter executer = new CommandLineExecuter();
		String response = executer.executeCommand("echo", TEST_STRING);
		assertEquals(TEST_STRING, response);
	}

}
